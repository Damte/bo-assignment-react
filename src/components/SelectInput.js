import React, { useState, useEffect } from "react";
import Form from "react-bootstrap/Form";

function SelectInput({ onChange, accounts }) {
  const [state, setState] = useState(accounts[0].accountNumber);
  const handleSelect = (event) => {
    onChange(event.target.value);
    setState(event.target.value);
  };
  useEffect(() => {}, [state]);

  return (
    <>
      <Form.Control
        className="col-4 "
        as="select"
        name="version"
        onChange={handleSelect}
        value={state}
        custom
      >
        {accounts.map((account) => (
          <option value={account.accountNumber}>
            {" "}
            {account.accountNumber}
          </option>
        ))}
      </Form.Control>
    </>
  );
}

export default SelectInput;
