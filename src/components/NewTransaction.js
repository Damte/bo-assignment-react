import React, { useState, useEffect, useRef } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import SelectInput from "./SelectInput";
import useDidMountEffect from "./customHooks/useDidMountEffect";
import "./TransactionMain.css";
import { useHistory } from "react-router-dom";
import useForm from "./customHooks/useForm";
import validate from "./validation/TransactionFormValidationRules";

function Transaction() {
  const [accounts, setAccounts] = useState([
    {
      createdAt: "",
      updatedAt: "",
      active: "",
      id: "",
      accountNumber: "",
      currentBalance: "",
    },
  ]);
  const [transaction, setTransaction] = useState({});
  const [errorsBE, setErrorsBE] = useState({});
  const [inputFieldBlurred, setInputFieldBlurred] = useState(false);
  const [state, setState] = useState({
    firstName: "",
    destination: "",
    concept: "",
    origin: "",
  });
  const [selectedOption, setSelectedOption] = useState("");
  function handleOptionChange(newName) {
    setSelectedOption(newName);
    setInputFieldBlurred(!inputFieldBlurred);
    setTransaction({
      ...transaction,
      account: {
        accountNumber: newName,
      },
    });
  }

  function handleInputChange(evt) {
    const value = evt.target.value;
    setState({
      ...state,
      [evt.target.name]: value,
    });
    setInputFieldBlurred(!inputFieldBlurred);
    handleChange(evt);
  }

  const firstUpdate = useRef(true);
  useDidMountEffect(() => {
    // if (firstUpdate.current) {
    //     firstUpdate.current = false;
    //     return;
    // } else {
    if (transaction.account.accountNumber === "") {
      setTransaction({
        account: {
          accountNumber: accounts[0].accountNumber,
        },
        destinationAccount: {
          accountNumber: state.destination,
        },
        transactionAmount: state.amount,
        description: state.concept,
      });
    } else {
      setTransaction({
        ...transaction,

        destinationAccount: {
          accountNumber: state.destination,
        },
        transactionAmount: state.amount,
        description: state.concept,
      });
    }
    // }
  }, [inputFieldBlurred]);

  const optionsJSON = {
    method: "POST",
    body: JSON.stringify(transaction),
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.token}`,
    },
  };

  useDidMountEffect(() => {
    if (typeof errorsBE.message !== "undefined") {
      alert(errorsBE.message);
    }
    if (typeof errorsBE.message === "undefined") {
      alert("Transfer has been submitted, please check your account balance");
    }
  }, [errorsBE]);

  useEffect(() => {
    fetchAccounts();
    setTransaction({
      ...transaction,
      account: {
        accountNumber: accounts[0].accountNumber,
      },
    });
  }, []);
  const fetchAccounts = async () => {
    let headers = {};
    if (localStorage.token) {
      headers = {
        Authorization: `Bearer ${localStorage.token}`,
      };
    }
    const data = await fetch(
      `http://localhost:8080/customer/${localStorage.customerId}/accounts`,
      { headers: headers }
    );
    const json = await data.json();
    setAccounts(json);
  };

  function handleErrors(response) {
    if (!response.ok) {
      setErrorsBE(response);
      throw Error(response);
    }
  }

  const history = useHistory();
  const handleClick = (e) => {
    doTransaction();
    // history.push(`/customer/accounts`)
  };

  const doTransaction = async () => {
    try {
      const data = await fetch(
        "http://localhost:8080/transaction",
        optionsJSON
      );

      const json = await data.json();
      handleErrors(json);
    } catch (err) {
      console.log(`${errorsBE.message} inside catch`);
      // alert(`${errors.message} inside catch`);
    }
  };

  const { values, errors, handleChange, handleSubmit } = useForm(
    handleClick,
    validate
  );

  if (typeof accounts === "undefined") {
    return null;
  }
  return (
    <>
      <br />
      <br />
      <section>
        <div className="container">
          <div className="row">
            <div className="col-md-8 offset-md-2 mb-4">
              <h1 className=" text-capitalize">Create new transaction</h1>
            </div>
          </div>
        </div>
      </section>
      <Form className="container" onSubmit={handleSubmit} noValidate>
        <Form.Group
          inline
          controlId="exampleForm.SelectCustom"
          className="text-left text-capitalize h4"
        >
          <span className="row">
            <Form.Label className="text-capitalize col-4 offset-2">
              Select origin account:{" "}
            </Form.Label>
            <SelectInput onChange={handleOptionChange} accounts={accounts} />
          </span>
          <div className="row">
            {errors.destination && (
              <small className="text-danger offset-6">
                {errors.destination}
              </small>
            )}
          </div>
          <span className="row">
            <Form.Label className="col-4 offset-2">
              Enter destination account:
            </Form.Label>
            <Form.Control
              className="col-4"
              type="text"
              name="destination"
              value={values.destination || ""}
              onChange={handleInputChange}
              placeholder="XX00 0000 0000 0000"
            ></Form.Control>
          </span>
          <div className="row">
            {errors.amount && (
              <small className="text-danger offset-6">{errors.amount}</small>
            )}
          </div>
          <span className="row">
            <Form.Label className="offset-2 col-4">
              Enter amount of money:
            </Form.Label>
            <Form.Control
              className="col-4 "
              type="text"
              placeholder="1000"
              name="amount"
              value={values.amount || ""}
              onChange={handleInputChange}
            />
          </span>
          <div className="row">
            {errors.concept && (
              <small className="text-danger offset-6">{errors.concept}</small>
            )}
          </div>
          <span className="row">
            <Form.Label className="offset-2 col-4">
              Transfer description:
            </Form.Label>
            <Form.Control
              className="col-4 "
              type="text"
              placeholder="Concept"
              name="concept"
              value={values.concept || ""}
              onChange={handleInputChange}
            />
          </span>
          <Button
            className="mt-2 offset-6 col-4"
            type="submit"
            block
            variant="outline-dark"
            //   onClick={handleClick}
          >
            Submit
          </Button>
        </Form.Group>
      </Form>
      <br />
      <br />
    </>
  );
}

export default Transaction;
