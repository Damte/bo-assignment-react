import React from "react";
import SignUpForm from "./SignUpForm";

function Signup() {
  return (
    <>
      <section id="sign-up" className="sign-up bg-dark">
        <div className="container">
          <div className="row">
            <div className="col-md-8 offset-md-2 mb-2">
              <h1 className=" text-uppercase text-light">Sign Up</h1>
              <SignUpForm />
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default Signup;
